package ru.moysklad;

import ru.moysklad.dfs.DFS;
import ru.moysklad.graph.Graph;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Екатерина on 30.04.2016.
 */
public class CycleFinderMain {
    public static void main(String[] args) {
        Graph graph = new Graph();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {

            String input;

            while ((input = reader.readLine()) != null && input.length() != 0) {
                String[] ids = input.split(" ");
                if (ids.length != 2) {
                    System.err.println("Uncorrect count of ids in row: " + input);
                    return;
                }
                graph.addEdge(Long.parseLong(ids[0]), Long.parseLong(ids[1]));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.err.println("Uncorrect data");
        }

        DFS dfsAlgo = new DFS(graph);
        dfsAlgo.searchCycles();
        dfsAlgo.printCycles();
    }
}
