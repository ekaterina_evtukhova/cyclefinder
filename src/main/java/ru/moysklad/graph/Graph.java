package ru.moysklad.graph;

import java.util.*;

/**
 * Created by Екатерина on 30.04.2016.
 */
public class Graph {

    private Map<Long, Vertex> vertexes = new HashMap<>();
    ;

    public Map<Long, Vertex> getVertexes() {
        return vertexes;
    }

    public Vertex getVertex(Long id) {
        return vertexes.get(id);
    }

    public void addEdge(Long src, Long dest) {
        Vertex srcVertex = new Vertex(src);
        srcVertex.addDest(dest);

        if (!vertexes.containsKey(src)) {
            vertexes.put(src, srcVertex);
        } else {
            vertexes.get(src).addDest(dest);
        }

        if (!vertexes.containsKey(dest)) {
            vertexes.put(dest, new Vertex(dest));
        }
    }
}
