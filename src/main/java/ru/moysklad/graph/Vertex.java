package ru.moysklad.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Екатерина on 30.04.2016.
 */
public class Vertex {
    public enum VertexColor {
        WHITE, BLACK
    }

    private Long id;
    private Set<Long> edges = new HashSet<>();
    private VertexColor color;


    public VertexColor getColor() {
        return color;
    }

    public void setColor(VertexColor color) {
        this.color = color;
    }

    public Set<Long> getEdges() {
        return edges;
    }

    public Vertex(Long id) {
        this.id = id;
        this.color = VertexColor.WHITE;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addDest(Long dest) {
        edges.add(dest);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vertex vertex = (Vertex) o;

        return id.equals(vertex.id);

    }
}
