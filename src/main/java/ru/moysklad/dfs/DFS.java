package ru.moysklad.dfs;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import ru.moysklad.graph.Graph;
import ru.moysklad.graph.Vertex;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Екатерина on 30.04.2016.
 */
public class DFS {
    private List<LinkedList<Long>> cycles = new ArrayList<>();
    private Graph graph;

    public DFS(Graph graph) {
        this.graph = graph;
    }

    public void printCycles() {
        if (cycles.size() == 0) {
            System.out.println("No cycles found");
        } else for (LinkedList<Long> cycle : cycles) {
            System.out.println(Joiner.on(" ").join(cycle));
        }
    }

    public void searchCycles() {
        for (Vertex v : graph.getVertexes().values()) {
            if (v.getColor() == Vertex.VertexColor.WHITE) {
                LinkedList<Long> potentialCycle = new LinkedList<>();
                potentialCycle.add(v.getId());
                dfs(v, potentialCycle);
            }
        }
    }

    private boolean dfs(Vertex u, LinkedList<Long> potentialCycle) {
        u.setColor(Vertex.VertexColor.BLACK);

        for (Long id : u.getEdges()) {
            Vertex w = graph.getVertex(id);
            if (w.getColor() == Vertex.VertexColor.WHITE) {
                potentialCycle.add(w.getId());
                if (dfs(w, potentialCycle))
                    potentialCycle.remove(w.getId());
            } else {
                int index = potentialCycle.indexOf(w.getId());
                potentialCycle.add(w.getId());

                LinkedList<Long> tmp = new LinkedList<>();
                for (Long v : w.getEdges()) {
                    if (graph.getVertex(v).getColor() == Vertex.VertexColor.BLACK
                            && v.equals(potentialCycle.getFirst())) {
                        tmp.add(v);
                    }
                }

                if (tmp.isEmpty() && index != -1) {
                    cycles.add(new LinkedList<>(potentialCycle.subList(index, potentialCycle.size())));
                } else if (!tmp.isEmpty()) {
                    cycles.add(Lists.newLinkedList(Iterables.concat(potentialCycle, tmp)));
                }

                potentialCycle.removeLast();
                return true;
            }
        }

        potentialCycle.removeLast();
        return false;
    }
}
