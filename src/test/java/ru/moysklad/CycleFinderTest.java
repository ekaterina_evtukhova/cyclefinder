package ru.moysklad;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static ru.moysklad.RequestResponse.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by Екатерина on 01.05.2016.
 */
public class CycleFinderTest {
    private final ByteArrayOutputStream systemOut = new ByteArrayOutputStream();
    private final ByteArrayOutputStream systemErr = new ByteArrayOutputStream();

    @BeforeClass
    public static void setupSystemParams() {
        System.setProperty("line.separator","\r\n");
    }

    @Before
    public void setUp() {
        System.setOut(new PrintStream(systemOut));
        System.setErr(new PrintStream(systemErr));
    }

    @After
    public void tearDown() {
        System.setIn(null);
        System.setOut(null);
        System.setErr(null);
    }

    private void mainTest() {
        Scanner scanner = new Scanner(System.in);
        CycleFinderMain.main(new String[]{});
        while (scanner.hasNext()) {
            scanner.nextLine();
        }
    }

    @Test
    public void testEmptyInput() {
        System.setIn(IOUtils.toInputStream(INPUT_EMPTY));
        mainTest();
        assertEquals(OUTPUT_EMRTY, systemOut.toString());
    }

    @Test
    public void testUncorrecNumberOfNumbers1() {
        System.setIn(IOUtils.toInputStream(INPUT_UNCORRECT_COUNT_OF_NUMBERS_1));
        mainTest();
        assertEquals(OUTPUT_UNCORRECT_COUNT_OF_NUMBERS_1, systemErr.toString());
    }

    @Test
    public void testUncorrecNumberOfNumbers2() {
        System.setIn(IOUtils.toInputStream(INPUT_UNCORRECT_COUNT_OF_NUMBERS_2));
        mainTest();
        assertEquals(OUTPUT_UNCORRECT_COUNT_OF_NUMBERS_2, systemErr.toString());
    }

    @Test
    public void testUncorrecSymbol() {
        System.setIn(IOUtils.toInputStream(INPUT_UNCORRECT_SYMBOL));
        mainTest();
        assertEquals(OUTPUT_UNCORRECT_SYMBOL, systemErr.toString());
    }

    @Test
    public void testWithoutCycles() {
        System.setIn(IOUtils.toInputStream(INPUT_WITHOUT_CYCLES));
        mainTest();
        assertEquals(OUTPUT_WITHOUT_CYCLES, systemOut.toString());
    }

    @Test
    public void testOneCycle() {
        System.setIn(IOUtils.toInputStream(INPUT_ONE_CYCLE));
        mainTest();
        assertEquals(OUTPUT_ONE_CYCLE, systemOut.toString());
    }

    @Test
    public void testFromExample() {
        System.setIn(IOUtils.toInputStream(INPUT_FROM_EXAMPLE));
        mainTest();
        assertEquals(OUTPUT_FROM_EXAMPLE, systemOut.toString());
    }

    @Test
    public void testWithDublicates() {
        System.setIn(IOUtils.toInputStream(INPUT_WITH_DUBLICATES));
        mainTest();
        assertEquals(OUTPUT_WITH_DUBLICATES, systemOut.toString());
    }

    @Test
    public void testOneCommonVertex() {
        System.setIn(IOUtils.toInputStream(INPUT_ONE_COMMON_VERTEX));
        mainTest();
        assertEquals(OUTPUT_ONE_COMMON_VERTEX, systemOut.toString());
    }

    @Test
    public void testSeveralCommonVertex() {
        System.setIn(IOUtils.toInputStream(INPUT_SEVERAL_COMMON_VERTEX));
        mainTest();
        assertEquals(OUTPUT_SEVERAL_COMMON_VERTEX, systemOut.toString());
    }

    @Test
    public void testAllCommonVertex() {
        System.setIn(IOUtils.toInputStream(INPUT_ALL_COMMON_VERTEX));
        mainTest();
        assertEquals(OUTPUT_ALL_COMMON_VERTEX, systemOut.toString());
    }

    @Test
    public void testSeveralCyclesWithoutCommonVertex() {
        System.setIn(IOUtils.toInputStream(INPUT_SEVERAL_CYCLES_WITHOUT_COMMON_VERTEX));
        mainTest();
        assertEquals(OUTPUT_SEVERAL_CYCLES_WITHOUT_COMMON_VERTEX, systemOut.toString());
    }
}
