package ru.moysklad;

/**
 * Created by Екатерина on 01.05.2016.
 */
public class RequestResponse {

    public static final String INPUT_EMPTY = "\r\n";
    public static final String OUTPUT_EMRTY = "No cycles found\r\n";

    public static final String INPUT_UNCORRECT_COUNT_OF_NUMBERS_1 = "1\r\n";
    public static final String OUTPUT_UNCORRECT_COUNT_OF_NUMBERS_1 = "Uncorrect count of ids in row: 1\r\n";

    public static final String INPUT_UNCORRECT_COUNT_OF_NUMBERS_2 = "1 2 3\r\n";
    public static final String OUTPUT_UNCORRECT_COUNT_OF_NUMBERS_2 = "Uncorrect count of ids in row: 1 2 3\r\n";

    public static final String INPUT_UNCORRECT_SYMBOL = "1 a\r\n";
    public static final String OUTPUT_UNCORRECT_SYMBOL = "Uncorrect data\r\n";

    public static final String INPUT_WITHOUT_CYCLES = "1 2\r\n2 3\r\n5 4\r\n";
    public static final String OUTPUT_WITHOUT_CYCLES = "No cycles found\r\n";

    public static final String INPUT_ONE_CYCLE = "1 2\r\n2 3\r\n3 4\r\n4 1\r\n";
    public static final String OUTPUT_ONE_CYCLE = "1 2 3 4 1\r\n";

    public static final String INPUT_FROM_EXAMPLE = "1 2\r\n2 1\r\n3 4\r\n5 6\r\n6 5\r\n";
    public static final String OUTPUT_FROM_EXAMPLE = "1 2 1\r\n5 6 5\r\n";

    public static final String INPUT_WITH_DUBLICATES = "1 2\r\n2 1\r\n1 2\r\n1 2\r\n";
    public static final String OUTPUT_WITH_DUBLICATES = "1 2 1\r\n";

    public static final String INPUT_ONE_COMMON_VERTEX = "1 2\r\n2 3\r\n3 4\r\n4 1\r\n2 5\r\n5 2\r\n";
    public static final String OUTPUT_ONE_COMMON_VERTEX = "1 2 3 4 1\r\n2 5 2\r\n";

    public static final String INPUT_SEVERAL_COMMON_VERTEX = "1 2\r\n2 3\r\n3 4\r\n4 1\r\n2 5\r\n5 4\r\n";
    public static final String OUTPUT_SEVERAL_COMMON_VERTEX = "1 2 3 4 1\r\n1 2 5 4 1\r\n";

    public static final String INPUT_ALL_COMMON_VERTEX = "1 2\r\n2 3\r\n3 2\r\n2 1\r\n";
    public static final String OUTPUT_ALL_COMMON_VERTEX = "1 2 1\r\n3 2 3\r\n";

    public static final String INPUT_SEVERAL_CYCLES_WITHOUT_COMMON_VERTEX =
            "1 2\r\n2 3\r\n3 1\r\n2 4\r\n4 5\r\n5 6\r\n6 4\r\n5 7\r\n7 8\r\n8 9\r\n9 7\r\n";
    public static final String OUTPUT_SEVERAL_CYCLES_WITHOUT_COMMON_VERTEX =
            "1 2 3 1\r\n4 5 6 4\r\n7 8 9 7\r\n";
}
